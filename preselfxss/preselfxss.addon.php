<?php

if(!defined('__XE__')) exit();

/**
 * @file preselfxss.addon.php
 * @author Podongi (podongi@outlook.kr)
 * @brief Display Self-XSS prevention code.
 */

Context::addHtmlHeader($presxcs);

$presxcs="
<script>
	console.log('%c조심하세요!', 'color:red; background:yellow; font-size:40px;')
	console.log('%cConsole은 개발자용으로 브라우저에서 제공하는 기능입니다. 누군가 Console에 어떤 코드를 붙여넣으라고 한다면 Self-XSS라는 공격을 통해 이용자의 계정 정보를 탈취하거나 조작하기 위한 목적일 수 있습니다.','font-size:15px;')
	console.log('%c어떤 역할을 하는지 자세히 모르는 코드를 Console에 입력하거나 붙여넣지 마세요. ','font-size:15px;')
</script>";
